class CreateMicroposts < ActiveRecord::Migration
  def change
    create_table :microposts do |t|
      t.string :content
      t.integer :user_id
      
      #addds the created_at and updated_at columns
      t.timestamps
    end
    # with both in an array, ActiveRecord uses both
    # keys at the same time in a multiple key index
    add_index :microposts, [:user_id, :created_at]
  end
end
